<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuarios;

class UsuariosController extends Controller
{
    public function index()
    {
        return Usuarios::all();
    }
 
    public function show($id)
    {
        return Usuarios::find($id);
    }

    public function store(Request $request)
    {
        return Usuarios::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $usuarios = Usuarios::findOrFail($id);
        $usuarios->update($request->all());

        return $usuarios;
    }

    public function delete(Request $request, $id)
    {
        $usuarios = Usuarios::findOrFail($id);
        $usuarios->delete();

        return 204;
    }
}
