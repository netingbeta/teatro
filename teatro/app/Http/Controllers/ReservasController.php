<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reservas;

class ReservasController extends Controller
{
    public function index()
    {
        return Reservas::all();
    }
 
    public function show($id)
    {
        return Reservas::find($id);
    }

    public function store(Request $request)
    {
        return Reservas::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $reservas = Reservas::findOrFail($id);
        $reservas->update($request->all());

        return $reservas;
    }

    public function delete(Request $request, $id)
    {
        $reservas = Reservas::findOrFail($id);
        $reservas->delete();

        return 204;
    }
}
