<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::get('usuarios', 'UsuariosController@index');
Route::get('usuarios/{id}', 'UsuariosController@show');
Route::post('usuarios', 'UsuariosController@store');
Route::put('usuarios/{id}', 'UsuariosController@update');
Route::delete('usuarios/{id}', 'UsuariosController@delete');


Route::get('reservas', 'ReservasController@index');
Route::get('reservas/{id}', 'ReservasController@show');
Route::post('reservas', 'ReservasController@store');
Route::put('reservas/{id}', 'ReservasController@update');
Route::delete('reservas/{id}', 'ReservasController@delete');
